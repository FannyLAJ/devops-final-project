# DevOPS Final Project

Devops final project 

# Procédure de déploiement
## Prérequis ##
3 VMs CentOS avec accès SSH en root autorisé et certificats installés

Ansible 

Une connexion Internet

## Étape 1 ##
Cloner ou télécharger les sources sur le poste local

## Étape 2 ##
Exécuter le playbook "playbook-docker.yml"
