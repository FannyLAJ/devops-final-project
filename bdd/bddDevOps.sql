/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     16/12/2018 14:51:30                          */
/*==============================================================*/

drop database if exists devops;

create database devops;

use devops;

drop table if exists USER;
drop table if exists URL;

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/

create table USER
(
	id_user 	int auto_increment,
	username	varchar (255) unique,
	password 	varchar (255),
	mail 		varchar (255),
	isadmin		bool,
	
	primary key (id_user)
);

/*==============================================================*/
/* Table: URL                                                   */
/*==============================================================*/

create table URL
(
	id_url 		tinyint auto_increment,
	url			varchar(255),
	id_user 	int,
	primary key (id_url)
);
alter table URL add constraint FK_USER foreign key (id_user)
      references USER (id_user) on delete restrict on update restrict;