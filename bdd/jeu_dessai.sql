use devops;

/*==============================================================*/
/* Données: USER                                                */
/*==============================================================*/

insert into user (username,password,mail,isadmin) values
('Yserdetest','yserdetest','yserdetest@yopmail.com',false),
('YserdetestAdmin','yserdetestadmin','ysertdetestadmin@yopmail.com',true);

/*==============================================================*/
/* Données: URL                                                 */
/*==============================================================*/

insert into url (url) values
('https://www.google.com/search?client=firefox-b&q=test',1);