//TestApi.js
//Author : Nicolas LETOCART

const request = require('supertest');

const app = require ('../index.js');

//==================== Test for API ====================

/**
 * Testing login route
 */
describe('GET /login', function () {
    it('respond with json containing the name, the password and the statut(admin or not) of the user', function (done) {
        request(app)
            .get('/login/:user/:password')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

/**
 * Testing create account route
 */
describe('POST /signup', function () {
	let data={
		user='test1',
		password='test1',
		email='test1@yopmail.com'
	}
    it('respond with code 201 if an account is created', function (done) {
        request(app)
            .post('/signup/:user/:password/:email')
			.send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201, done);
			.end((err)=>{
				if(err) return done(err);
				done();
			});
    });
});
/**
 * Testing delete user route
 */
describe('DELETE /user', function () {
	let data={
		userId="1"
	}
    it('respond with code 200', function (done) {
        request(app)
            .delete('/user/:userId')
			.send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
/**
 * Testing add an url to a user route
 */
describe('POST /url', function () {
	let data={
		userId=3,
		url='https://www.google.com/search?client=firefox-b-ab&q=test'
	}
    it('respond with code 201', function (done) {
        request(app)
            .post('/url/:userId/:url')
			.send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201, done);
    });
});
/**
 * Testing get a user's url route
 */
describe('GET /url', function () {
	let data={
		userId=1
	}
    it("respond with json containing url's user", function (done) {
        request(app)
            .get('/url/:userId')
			.send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
/**
 * Testing delete a user's url route
 */
describe('DELETE /url', function () 
	let data={
		urlId=1
	}
    it('respond with code 200', function (done) {
        request(app)
            .delete('/url/:urlId')
			.send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});
