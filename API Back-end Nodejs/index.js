// The parameters to connect to the database

const MysqlParams = MysqlParams = {
    host: 'localhost',
    user: 'root',
    password : '',
    database : 'devops'
  };

// Import libraries
var express = require('express')
var app = express()

// Initialize the parameters to connect to the mysql database
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : MysqlParams.host,
  user     : MysqlParams.user,
  password : MysqlParams.password,
  database : MysqlParams.database
});
 
// Connect to the database 
connection.connect();

//***********************************Login Route****************************//
//**************************************************************************//
/**
 * Method:
 *      GET
 * Route:
 *      /login
 * Params:
 *      user (String)
 *      password (String)
 * Response:
 *      If the user exits and if he typed the right password:
 *          JsonString: Id_user: (Int), username: (String), isadmin: (Bool)
 *      If the user exists but specified the wrong password:
 *          String: "ERR1: Wrong password"
 *      If the user doesnt exists:
 *          String: "ERR0: No Entry Found"
 * 
 */
app.get('/login/:user/:password', function(req, res, next){
    var user = req.params.user;
    var password = req.params.password;
    // Do the query to select the users infos
    connection.query( "Select id_user, username, password, isadmin from USER WHERE username='"+user+"'", function(error, results, fields){
        // Throw error if problem during query
        if (error){
            throw error;
            // Send it to the client
            res.end(error);
        }else{
            // Check if the result contains some data. Else it means there is not entry for this query
            if(results.length > 0){
               
                results = results[0];

                if(password == results.password){
                    var resultat = {
                        id_user: results.id_user,
                        username: results.username,
                        isadmin: results.isadmin
                    };
                    res.end(JSON.stringify(resultat));
                }else{
                    res.end("ERR1: Wrong password");
                }
                //Construct an object with the results
                
                
                //Send it to the client
                
            }else{
                //Server's Response if there is no entry do the query
                res.end("ERR0: No Entry Found");
            }
           
        } 
    })
    
    
});


//*********************************Create Account Route**************************//
//*******************************************************************************//
/**
 * Method:
 *      POST
 * Route:
 *      /signup
 * Params:
 *      user (String)
 *      password (String)
 *      email (String)
 * Response:
 *      If the database throws an error:
 *          The Erro (String)
 *      If the database doesnt throw an error:
 *          [] (Array);
 */
app.post('/signup/:user/:password/:email', function(req, res, next){
    var user = req.params.user;
    var password = req.params.password;
    var email = req.params.email;
    
    connection.query( "INSERT into USER (username, password, mail, isadmin) VALUES('"+ user + "','"+ password +"','"+ email +"', 0)", function(error, results, fields){
        if (error){
            throw error;
            res.end(error);
        }else{
                results = results[0];
            
                res.end(JSON.stringify(results));
        } 
    })
    
});


//*********************************Delete User Route**************************//
//*******************************************************************************//
/**
 * Method:
 *      DELETE
 * Route:
 *      /user
 * Params:
 *      userId (Int)
 * Response:
 *      If the database throws an error:
 *          The Erro (String)
 *      If the database doesnt throw an error:
 *          [] (Array);
 */
app.delete('/user/:userId', function(req, res, next){
    var userId = req.params.userId;
    

    connection.query( "DELETE FROM USER WHERE id_user='"+ userId +"' ", function(error, results, fields){
        if (error){
            throw error;
            res.end(error);
        }else{
            results = results[0];
            
            res.end(JSON.stringify(results));
        } 
    })
});


//*********************************Add an Url to a User Route**************************//
//*******************************************************************************//
/**
 * Method:
 *      POST
 * Route:
 *      /url
 * Params:
 *      userId (Int)
 *      url (String)
 * Response:
 *      If the database throws an error:
 *          The Erro (String)
 *      If the database doesnt throw an error:
 *          [] (Array);
 */
app.post('/url/:userId/:url', function(req, res, next){
    var url = req.params.url;
    var userId = req.params.userId;

    connection.query( "INSERT INTO URL (url,id_user) VALUES ('"+ url +"', "+ userId +" );", function(error, results, fields){
        if (error){
            throw error;
            res.end(error);
        }else{
                results = results[0];
            
                res.end(JSON.stringify(results));
        } 
    })
    
});

//*********************************Get a User's Urls Route**************************//
//*******************************************************************************//
/**
 * Method:
 *      GET
 * Route:
 *      /url
 * Params:
 *      userId (Int)
 * Response:
 *      If the database throw an error:
 *          The Error (String)
 *      If there is urls:
 *          Send an Array with the results (Object)
 *      If there is no Url for the specified user:
 *          No Entry Found (String)
 */
app.get('/url/:userId', function(req, res, next){
    var userId = req.params.userId;
    

    connection.query( "SELECT url FROM USER,URL WHERE USER.id_user=URL.id_user AND USER.id_user="+ userId + "; ", function(error, results, fields){
        if (error){
            throw error;
            
            res.end(error);
        }else{
            
            if(results.length > 0){
                var resultat = [];
                for(var i = 0; i < results.length ; i ++){
                    
                     resultat.push({url: results[i].url}); 

                }

                res.end(JSON.stringify(resultat));

            }else{
                res.end("No Entry Found");
            }
        } 
    })
});

//*********************************DELETE a User's Url Route**************************//
//*******************************************************************************//
/**
 * Method:
 *      DELETE
 * Route:
 *      /url
 * Params:
 *      userId (Int)
 * Response:
 *     If the database throw an error:
 *          Send the Error (String)
 *     If the database doesnt throw any error:
 *          [] (Array)
 */
app.delete('/url/:urlId', function(req, res, next){
    var urlId = req.params.urlId;
    var userId = req.params.userId;

    connection.query( "DELETE FROM URL WHERE id_url='"+ urlId +"'", function(error, results, fields){
        if (error){
            throw error;
            res.end(error);
        }else{
                results = results[0];
            
                res.end(JSON.stringify(results));
        } 
    })
});

// Start The server on the 3000 port
app.listen(3000, function(){
    console.log('The Started on the port 3000');
})
